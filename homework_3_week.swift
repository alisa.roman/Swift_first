import Foundation

struct Device {
    let name: String
    let screenSize: CGSize
    let screenDiagonal: Double
    let scaleFactor: Int
    
    static let iPhone14Pro = Device(name: "iPhone14Pro", screenSize: CGSize(width: 393, height: 852), screenDiagonal: 6.1, scaleFactor: 3)
    
    static let iPhoneXR = Device(name: "iPhoneXR", screenSize: CGSize(width: 414, height: 896), screenDiagonal: 6.06, scaleFactor: 2)
    
    static let iPadPro = Device(name: "iPadPro", screenSize: CGSize(width: 834, height: 1194), screenDiagonal: 11.0, scaleFactor: 2)
    
    func physicalSize() -> CGSize {
            return CGSize(
                width: screenSize.width * CGFloat(scaleFactor),
                height: screenSize.height * CGFloat(scaleFactor)
            )
    }
}

print(Device.iPhone14Pro.physicalSize()) // (1179.0, 2556.0)
print(Device.iPhoneXR.physicalSize()) // (828.0, 1792.0)
print(Device.iPadPro.physicalSize()) // (1668.0, 2388.0)
