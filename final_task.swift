import Foundation

protocol CalculatorProtocol {
    func calculate(action actionType: Action, _ a: Double, _ b: Double) -> Double?
}

enum Action {
    case add
    case subtract
    case multiply
    case divide
    case toPower
    case sqRoot
    case showPi
    case changeSign
}

class CalculatorSimple: CalculatorProtocol {
    private let errorMessage: String = "Введенная операция не поддержидается простым калькулятором"
    
    public func calculate(action actionType: Action, _ a: Double, _ b: Double) -> Double? {
        switch actionType {
        case .add:
            return a + b
        case .subtract:
            return a - b
        case .multiply:
            return a * b
        case .divide:
            if b == 0 {
                print("Деление на 0 запрещено")
            }
            return a / b
        default:
            print(errorMessage)
            return nil
        }
    }
}

final class CalculatorAdvanced: CalculatorSimple {
    private let errorMessage: String = "Введенная операция не поддержидается расширенным калькулятором"
    
    override public func calculate(action actionType: Action, _ a: Double, _ b: Double) -> Double? {
        switch actionType {
        case .add, .subtract, .multiply, .divide:
            return super.calculate(action: actionType, a, b)
        case .toPower:
            return pow(a, b)
        default:
            print(errorMessage)
            return nil
        }
    }

// перегрезка метода, чтобы принимал только 1 переменную и действие
    public func calculate(action actionType: Action, _ a: Double) -> Double? {
        switch actionType {
        case .sqRoot:
            return sqrt(a)
        case .changeSign:
            return -a
        default:
            print(errorMessage)
            return nil
        }
    }
    
// перегрезка метода, чтобы принимал только действие
    public func calculate(action actionType: Action) -> Double? {
        switch actionType {
        case .showPi:
            return Double.pi
        default:
            print(errorMessage)
            return nil
        }
    }
}


var simpleCalculator = CalculatorSimple()
simpleCalculator.calculate(action: .add, 5, 6) // 11
simpleCalculator.calculate(action: .subtract, 5, 6) // -1
simpleCalculator.calculate(action: .multiply, 5, 6) // 30
simpleCalculator.calculate(action: .divide, 5, 6) // 0.8(3)
simpleCalculator.calculate(action: .toPower, 5, 6) // nil : Введеная операция не поддержидается простым калькулятором

var advancedCalculator = CalculatorAdvanced()
advancedCalculator.calculate(action: .add, 11, 6) //17
advancedCalculator.calculate(action: .subtract, 5, 6) // -1
advancedCalculator.calculate(action: .toPower, 5, 2) // 25
advancedCalculator.calculate(action: .showPi, 5, 2) // nil : Введенная операция не поддержидается расширенным калькулятором

advancedCalculator.calculate(action: .sqRoot, 5) // 2.2
advancedCalculator.calculate(action: .changeSign, 5) // -5

advancedCalculator.calculate(action: .showPi) // 3.14
