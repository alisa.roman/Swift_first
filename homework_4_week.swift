import Foundation

//Задание 1
class Shape {

    public func calculateArea() -> Double {
        fatalError("not implemented")
    }

    public func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
}

class Rectangle: Shape {
    private let width: Double
    private let height: Double

    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }

    override public func calculateArea() -> Double {
        //вычисление площади прямоугольника по формуле S = a * b
        self.width * self.height
    }

    override public func calculatePerimeter() -> Double {
        //вычисление периметра прямоугольника по формуле P = 2 * (a + b)
        2 * (self.width + self.height)
    }
}

class Circle: Shape {
    private let radius: Double

    init(radius: Double) {
        self.radius = radius
    }

    override public func calculateArea() -> Double {
        //вычисление площади круга по формуле S = pi * r^2
        Double.pi * pow(radius, 2)
    }

    override public func calculatePerimeter() -> Double {
        //вычисление периметра круга по формуле P = 2 * pi * r
        2 * Double.pi * radius
    }
}

class Square: Shape {
    private let side: Double

    init(side: Double) {
        self.side = side
    }

    override public func calculateArea() -> Double {
        //вычисление площади квардата по формуле S = a^2
        pow(side, 2)
    }

    override public func calculatePerimeter() -> Double {
        //вычисление периметра квардата по формуле P = 4 * a
        4 * side
    }
}

let rectangle = Rectangle(width: 2, height: 3)
let circle = Circle(radius: 2)
let square = Square(side: 5)

var shapes: [Shape] = [rectangle, circle, square]

var areaTotalCount: Double = 0
var perimeterTotalCount: Double = 0

for shape in shapes{
    areaTotalCount += shape.calculateArea()
    perimeterTotalCount += shape.calculatePerimeter()
}

print("Общая площадь \(shapes.count) фигур: \(NSString(format: "%.2f", areaTotalCount))")
print("Общий периметр \(shapes.count) фигур: \(NSString(format: "%.2f", perimeterTotalCount))")


//Задание 2

func findIndexWithGenerics<T: Equatable>(valueToFind: T, in array: [T]) -> Int? {
    
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
let arrayOfInts = [1, 2, 3, 4, 5]
let arrayOfDoubles = [1.1, 2.1, 3.1, 4.1, 5.1]

if let foundTest = findIndexWithGenerics(valueToFind: "попугай", in: arrayOfString) {
    print("Индекс искомого элемента: \(foundTest)") // Индекс искомого элемента: 3
}

if let foundTest = findIndexWithGenerics(valueToFind: 3, in: arrayOfInts) {
    print("Индекс искомого элемента: \(foundTest)") // Индекс искомого элемента: 2
}

if let foundTest = findIndexWithGenerics(valueToFind: 2.1, in: arrayOfDoubles) {
    print("Индекс искомого элемента: \(foundTest)") // Индекс искомого элемента: 1
}
