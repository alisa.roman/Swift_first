import Foundation

//Задача 1
struct Candidate {
    let grade: Grade
    let requiredSalary: Int
    let fullName: String
    
    enum Grade {
        case junior
        case middle
        case senior
    }
}

protocol FilterCandidates {
    func filter(in array: [Candidate]) -> [Candidate]
}

struct FilterByGrade: FilterCandidates {
    let findGrade: Candidate.Grade
    
    func filter(in array: [Candidate]) -> [Candidate] {
        array.filter { candidate in candidate.grade == findGrade }
    }
}

struct FilterBySalary: FilterCandidates {
    let findSalary: Int
    
    func filter(in array: [Candidate]) -> [Candidate] {
        array.filter { candidate in candidate.requiredSalary <= findSalary }
    }
}

struct FilterByName: FilterCandidates {
    let findName: String
    
    func filter(in array: [Candidate]) -> [Candidate] {
        array.filter { candidate in candidate.fullName.contains(findName)
        }
    }
}

var allCandidates: [Candidate] = [
    Candidate(grade: .junior, requiredSalary: 100, fullName: "Копылов Василий Иванович"),
    Candidate(grade: .middle, requiredSalary: 50, fullName: "Капустина Алина Александровна"),
    Candidate(grade: .middle, requiredSalary: 200, fullName: "Бурый Петр Иванович"),
    Candidate(grade: .senior, requiredSalary: 500, fullName: "Осипов Иван Сергеевич")
]

//Тест кода
var filterResultsByGrade = FilterByGrade(findGrade: .middle).filter(in: allCandidates)
result(message: "Результат фильтрации по грейду:", array: filterResultsByGrade) // Капустина Алина Александровна и Бурый Петр Иванович

var filterResultsBySalary = FilterBySalary(findSalary: 199).filter(in: allCandidates)
result(message: "Результат фильтрации по зарплате:", array: filterResultsBySalary) // Копылов Василий Иванович и Капустина Алина Александровна

var filterResultsByName = FilterByName(findName: "Иванович").filter(in: allCandidates)
result(message: "Результат фильтрации по имени:", array: filterResultsByName) // Копылов Василий Иванович и Бурый Петр Иванович

//Функция для тестирования
func result(message: String, array: [Candidate]) {
    var result = [String]()
    
    for i in 0..<array.count {
        result.append(array[i].fullName)
    }
    print(message + " \(result)")
}

//Задача 2
extension Candidate.Grade {
    
    var gradeRank: Int {
        switch self {
        case .junior: return 1
        case .middle: return 2
        case .senior: return 3
        }
    }
}

struct FilterByMinGrade: FilterCandidates {
    let minGrade: Int
    
    func filter(in array: [Candidate]) -> [Candidate] {
        array.filter { candidate in candidate.grade.gradeRank >= minGrade }
    }
}

//Тест кода
let filerResultsByMinGrade = FilterByMinGrade(minGrade: 2).filter(in: allCandidates)
result(message: "Результат фильтрации по минимальному грейду и выше:", array: filerResultsByMinGrade) // Капустина Алина Александровна, Бурый Петр Иванович и Осипов Иван Сергеевич

