//Задание 1
var bufferArray = [String]()

var makeBuffer: (String) -> Void = {
    
    if $0 == "" {
        return print(bufferArray.joined(separator: " "))
    } else {
        return bufferArray.append($0)
    }
    
}

var buffer = makeBuffer
buffer("Hi")
buffer("reviewer")
buffer("")


//Задание 2
func checkPrimeNumber(number: Int) -> Bool {
    if number >= 2 {
        for i in 2 ..< number {
            if number % i == 0 {
                return false
            }
        }
        
        return true
    }
    
    return false
}

print("Result: \(checkPrimeNumber(number: 7))")
print("Result: \(checkPrimeNumber(number: 8))")
print("Result: \(checkPrimeNumber(number: 13))")

