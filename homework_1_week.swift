//Задача 1
let milkmanPhrase: String = "Молоко - это полезно"
print(milkmanPhrase)

//Задача 2
let updatedMilkPrice: Int = 3

//Задача 3
var milkPrice = 3.0
milkPrice = 4.20

//Задача 4
let milkBottleCount: Int? = 20
var profit: Double = 0.0
profit = milkPrice * Double(milkBottleCount!)
print(profit)

//Дополнительное задание 1
if let safeMilkBottleCount = milkBottleCount {
profit = milkPrice * Double(safeMilkBottleCount)
print(profit)
}

//Дополнительное задание 2
// Ответ: если используется принудительное развертывание, а значение переменной не придет, то возникнет ошибка в той части кода, где это значение используется
let catCount: Int? = nil
if let safeCatCount = catCount {
print(safeCatCount)
}

//Задача 5
var employeesList: [String]
employeesList = ["Иван", "Петр", "Геннадий"]
employeesList.append("Андрей")
employeesList.insert("Марфа", at: 4)

//Задача 6
var isEveryoneWorkHard: Bool = false
let workingHours: Int = 10
if workingHours >= 40 {
isEveryoneWorkHard = true
} else {
isEveryoneWorkHard = false
}
print(isEveryoneWorkHard)
